class HomeModel {
  final String id;
  final String name;
  final String price;
  final String size;
  final String image;
  

  HomeModel({required this.id,required this.name,required this.price,required this.size, required this.image});
}
