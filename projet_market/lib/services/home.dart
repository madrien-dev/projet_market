import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:projet_market/models/home.dart';

class HomeRepository {
  final FirebaseFirestore instanceFirestore;

  HomeRepository(this.instanceFirestore);

  Future<List<HomeModel>?> getAllDataHome() async {
    final List<HomeModel> dataHome = [];
    try {
      var data = await this.instanceFirestore.collection('homes').get();

      for (var doc in data.docs) {
        Map<String, dynamic> aa = doc.data();
        dataHome.add(HomeModel(
          id: doc.id,
          name: aa['name'],
          price: aa['price'],
          size: aa['price'],
          image: aa['image'],
        ));
      }
      return dataHome;
    } catch (e) {
      print(e);
    }
  }
}
