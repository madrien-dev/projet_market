import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:projet_market/models/users.dart';

class UsersRepository {
  final FirebaseFirestore instanceFirestore;

  UsersRepository(this.instanceFirestore);

  Future<UsersModel?> getUser(String doc) async {
    try {
      var data =
          await this.instanceFirestore.collection('users').doc(doc).get();

      Map<String, dynamic> aa = data.data() as Map<String, dynamic>;
      return UsersModel(aa['first_name'], aa['last_name'], aa['email']);
    } catch (e) {
      print(e);
    }
    //return {'first_name': 'Pierre', 'last_name': 'Chauvin'};
  }

  Future<void> updateUser(
      String firstName, String lastName, String doc) async {
    await this
        .instanceFirestore
        .collection('users')
        .doc(doc)
        .update(
          {'first_name': firstName, 'last_name': lastName},
        )
        .catchError((error) => print("Failed to add user: $error"));
  }

  Future<void> createUser(
      String firstName, String lastName, String uid, String email) async {
    await this
        .instanceFirestore
        .collection('users')
        .doc(uid)
        .set(
          {'first_name': firstName, 'last_name': lastName, 'email': email},
        )
        .catchError((error) => print("Failed to add user: $error"));
  }
}
