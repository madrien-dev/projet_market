import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:projet_market/models/home.dart';

class FavoritesRepository {
  final FirebaseFirestore instanceFirestore;

  FavoritesRepository(this.instanceFirestore);

  Future<void> addFavorite(String size, String price, String id, String image,
      String name, String uid) async {
    await this
        .instanceFirestore
        .collection('users')
        .doc(uid)
        .collection('favoris')
        .doc(id)
        .set({'size': size, 'price': price, 'image': image, 'name': name})
        .catchError((error) => print(error));
  }

  Future<List<HomeModel>?> getAllFavorites(String uid) async {
    final List<HomeModel> dataFavorite = [];
    try {
      var data = await this
          .instanceFirestore
          .collection('users')
          .doc(uid)
          .collection('favoris')
          .get();

      for (var doc in data.docs) {
        Map<String, dynamic> aa = doc.data();
        dataFavorite.add(HomeModel(
          id: doc.id,
          name: aa['name'],
          price: aa['price'],
          size: aa['price'],
          image: aa['image'],
        ));
      }
      return dataFavorite;
    } catch (e) {
      print(e);
    }
  }

  Future<void> deleteIdFavorite(String uid, String id) async {
    try {
      return await this
          .instanceFirestore
          .collection('users')
          .doc(uid)
          .collection('favoris')
          .doc(id)
          .delete();
    } catch (e) {
      print(e);
    }
  }
}
