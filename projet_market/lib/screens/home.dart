import 'package:flutter/material.dart';
import 'package:projet_market/screens/favorite.dart';
import 'package:projet_market/screens/home_picture.dart';
import 'package:projet_market/screens/profil.dart';
import 'package:projet_market/services/auth.dart';

class Home extends StatefulWidget {
  Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final AuthService _auth = AuthService();
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  List<Widget> _widgetOptions = <Widget>[
    PageView(
      scrollDirection: Axis.horizontal,
      children: [
        HomePicture(),
        Favorite(),
      ],
    ),
    Text(
      'Index 1: Recherche',
      style: optionStyle,
    ),
    Text(
      'Index 2: Panier',
      style: optionStyle,
    ),
    ProfilePage(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Market Flutter'), actions: <Widget>[
        TextButton.icon(
          icon: Icon(Icons.person, color: Colors.white),
          label: Text(
            'logout',
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () async {
            await _auth.signOut();
          },
        )
      ]),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: 'Recherche',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_basket_rounded),
            label: 'Panier',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_box),
            label: 'Compte',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ),
    );
  }
}
