import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:projet_market/models/user.dart';
import 'package:projet_market/models/users.dart';
import 'package:projet_market/services/users.dart';
import 'package:projet_market/widget/button_widget.dart';
import 'package:projet_market/widget/numbers_widget.dart';
import 'package:projet_market/widget/profile_widget.dart';
import 'package:provider/provider.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  String _lastName = '';
  String _firstName = '';
  String _email = '';
  bool _error = false;
  bool _edit = false;
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();

  UsersRepository users = UsersRepository(FirebaseFirestore.instance);

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<AppUser?>(context);

    return Scaffold(
      body: ListView(
        padding: const EdgeInsets.all(10),
        physics: BouncingScrollPhysics(),
        children: [
          ProfileWidget(
            imagePath:
                "https://media-exp1.licdn.com/dms/image/C5603AQGPOrVY9BgLdQ/profile-displayphoto-shrink_800_800/0/1580204071858?e=1641427200&v=beta&t=XNO4PnLQE5zoDteSTmHsZD7x0ebmhSUYTNs6JvDLz9I",
            onClicked: () async {
              if (_edit) {
                setState(() {
                  _edit = false;
                });
              } else {
                setState(() {
                  _edit = true;
                });
              }
            },
          ),
          const SizedBox(height: 24),
          Container(
            child: FutureBuilder(
              future: users.getUser(user!.uid),
              builder:
                  (BuildContext context, AsyncSnapshot<UsersModel?> snapshot) {
                if (snapshot.hasError) {
                  return Text("Something went wrong");
                }
                if (!snapshot.hasData) {
                  return Text('No data');
                }
                if (snapshot.connectionState == ConnectionState.done) {
                  _firstName = snapshot.data!.firstName;
                  _lastName = snapshot.data!.lastName;
                  _email = snapshot.data!.email;

                  return buildName(context, _firstName, _lastName, _email);
                }
                return Text("loading");
              },
            ),
          ),
          if (_edit)
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ConstrainedBox(
                  constraints: BoxConstraints.tightFor(width: 100, height: 40),
                  child: TextField(
                    controller: _lastNameController,
                    textAlignVertical: TextAlignVertical.bottom,
                    decoration: InputDecoration(
                      hintText: 'Nom',
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
                Padding(padding: EdgeInsets.symmetric(horizontal: 20.0)),
                ConstrainedBox(
                  constraints: BoxConstraints.tightFor(width: 100, height: 40),
                  child: TextField(
                    controller: _firstNameController,
                    textAlignVertical: TextAlignVertical.bottom,
                    decoration: InputDecoration(
                      hintText: 'Prénom',
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
              ],
            ),
          if (_edit)
            TextButton(
              style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all<Color>(Colors.blue),
              ),
              onPressed: () async {
                if (_firstNameController.text.toString().length > 0 &&
                    _lastNameController.text.toString().length > 0) {
                  await users.updateUser(_firstNameController.text.toString(),
                      _lastNameController.text.toString(), user.uid);
                  setState(() {
                    _firstName = _firstNameController.text.toString();
                    _lastName = _lastNameController.text.toString();
                    _edit = false;
                  });
                }
              },
              child: Text('Changer mon nom'),
            ),
          const SizedBox(height: 24),
          Center(child: buildUpgradeButton()),
          const SizedBox(height: 24),
          NumbersWidget(),
        ],
      ),
    );
  }

  Widget buildName(BuildContext context, firstName, lastName, email) => Column(
        children: [
          Text(
            "$firstName $lastName",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
          ),
          const SizedBox(height: 4),
          Text(
            email,
            style: TextStyle(color: Colors.grey),
          )
        ],
      );

  Widget buildUpgradeButton() => ButtonWidget(
        text: 'Upgrade To PRO',
        onClicked: () {},
      );
}
