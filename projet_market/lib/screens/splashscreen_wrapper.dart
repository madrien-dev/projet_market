import 'package:flutter/material.dart';
import 'package:projet_market/models/user.dart';
import 'package:projet_market/screens/authentification.dart';
import 'package:projet_market/screens/home.dart';
import 'package:provider/provider.dart';

class SplashScreenWrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<AppUser?>(context);
    if (user == null) {
      return AuthenticateScreen();
    } else {
      return Home();
    }
  }
}