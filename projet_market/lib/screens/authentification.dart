import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:projet_market/common/constants.dart';
import 'package:projet_market/common/loading.dart';
import 'package:projet_market/services/auth.dart';
import 'package:projet_market/services/users.dart';

class AuthenticateScreen extends StatefulWidget {
  @override
  _AuthenticateScreenState createState() => _AuthenticateScreenState();
}

class _AuthenticateScreenState extends State<AuthenticateScreen> {
  final AuthService _auth = AuthService();
  UsersRepository _users = UsersRepository(FirebaseFirestore.instance);

  final _formKey = GlobalKey<FormState>();
  String error = '';
  bool loading = false;

  final emailController = TextEditingController();
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final passwordController = TextEditingController();
  bool _isSecret = true;

  bool showSignIn = true;

  @override
  void dispose() {
    firstNameController.dispose();
    lastNameController.dispose();
    emailController.dispose();
    passwordController.dispose();

    super.dispose();
  }

  void toggleView() {
    setState(() {
      _formKey.currentState?.reset();
      error = '';
      emailController.text = '';
      lastNameController.text = '';
      passwordController.text = '';
      showSignIn = !showSignIn;
    });
  }

  @override
  Widget build(BuildContext context) {
    return loading
        ? Loading()
        : Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              backgroundColor: Colors.blueGrey,
              elevation: 0.0,
              title: Text(showSignIn ? 'Se Connecter' : 'Créer Compte'),
              actions: <Widget>[
                TextButton.icon(
                  icon: Icon(
                    Icons.person,
                    color: Colors.white,
                  ),
                  label: Text(showSignIn ? "Créer Compte" : 'Se Connecter',
                      style: TextStyle(color: Colors.white)),
                  onPressed: () => toggleView(),
                ),
              ],
            ),
            body: Container(
              padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 30.0),
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    if (!showSignIn)
                      TextFormField(
                        controller: lastNameController,
                        decoration:
                            textInputDecoration.copyWith(hintText: 'nom'),
                        validator: (value) => value == null || value.isEmpty
                            ? "Entrer un nom"
                            : null,
                      ),
                    SizedBox(height: 10.0),
                    if (!showSignIn)
                      TextFormField(
                        controller: firstNameController,
                        decoration:
                            textInputDecoration.copyWith(hintText: 'prénom'),
                        validator: (value) => value == null || value.isEmpty
                            ? "Entrer un prénom"
                            : null,
                      ),
                    SizedBox(height: 10.0),
                    TextFormField(
                      controller: emailController,
                      decoration:
                          textInputDecoration.copyWith(hintText: 'email'),
                      validator: (value) => value == null || value.isEmpty
                          ? "Enter votre email"
                          : null,
                    ),
                    SizedBox(height: 10.0),
                    TextFormField(
                      controller: passwordController,
                      decoration: InputDecoration(
                        labelText: 'Mot de Passe',
                        suffixIcon: InkWell(
                          onTap: () => setState(() => _isSecret = !_isSecret),
                          child: Icon(
                            !_isSecret
                                ? Icons.visibility
                                : Icons.visibility_off,
                          ),
                        ),
                        fillColor: Colors.white,
                        filled: true,
                        contentPadding: EdgeInsets.all(12.0),
                        errorBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.red, width: 1.0),
                        ),
                        focusedErrorBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.red, width: 1.0),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.blueGrey, width: 1.0),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.blue, width: 1.0),
                        ),
                      ),
                      obscureText: _isSecret,
                      validator: (value) => value != null && value.length < 6
                          ? "Entrer pour le mot de passe au moins 6 caractères"
                          : null,
                    ),
                    SizedBox(height: 10.0),
                    ElevatedButton(
                      child: Text(
                        showSignIn ? "Se Connecter" : "Créer Compte",
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () async {
                        try {
                          if (_formKey.currentState?.validate() == true) {
                            dynamic result;
                            setState(() => loading = true);
                            var password = passwordController.value.text;
                            var email = emailController.value.text;
                            if (showSignIn) {
                              result = await _auth.signInWithEmailAndPassword(
                                  email, password);
                            } else {
                              result = await _auth.registerWithEmailAndPassword(
                                  email, password);

                              _users.createUser(
                                  firstNameController.text,
                                  lastNameController.text,
                                  result.uid,
                                  emailController.text);
                            }

                            if (result == null) {
                              setState(() {
                                loading = false;
                                error =
                                    'Entre une adresse email correct s\'il vous plaît';
                              });
                            }
                          }
                        } catch (e) {
                          setState(() {
                            loading = false;
                            error =
                                'Entre une adresse email correct s\'il vous plaît';
                          });
                        }
                      },
                    ),
                    SizedBox(height: 10.0),
                    Text(
                      error,
                      style: TextStyle(color: Colors.red, fontSize: 15.0),
                    )
                  ],
                ),
              ),
            ),
          );
  }
}
