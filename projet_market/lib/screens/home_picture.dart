import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:projet_market/models/home.dart';
import 'package:projet_market/models/user.dart';
import 'package:projet_market/services/favorites.dart';
import 'package:projet_market/services/home.dart';
import 'package:provider/provider.dart';

class HomePicture extends StatefulWidget {
  const HomePicture({Key? key}) : super(key: key);

  @override
  _HomePictureState createState() => _HomePictureState();
}

class _HomePictureState extends State<HomePicture> {
  HomeRepository _home = HomeRepository(FirebaseFirestore.instance);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: _home.getAllDataHome(),
        builder:
            (BuildContext context, AsyncSnapshot<List<HomeModel>?> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.data!.length < 1) {
              return Container(
                child: Center(
                  child: Text('Aucun produit en vente'),
                ),
              );
            } else {
              return ListView.builder(
                itemCount: snapshot.data!.length,
                itemBuilder: (context, index) {
                  return _buildCard(context, snapshot.data![index].image, index,
                      snapshot.data!);
                },
              );
            }
          } else {
            return Container(
              child: Center(
                child: Text('Aucun produit en vente'),
              ),
            );
          }
        },
      ),
    );
  }
}

Widget _buildCard(BuildContext context, img, i, info) {
  final user = Provider.of<AppUser?>(context);
  FavoritesRepository _favorite =
      FavoritesRepository(FirebaseFirestore.instance);
  return GestureDetector(
    onDoubleTap: () => {
      _favorite.addFavorite(info[i].size, info[i].price, info[i].id,
          info[i].image, info[i].name, user!.uid),
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: const Text('Produit ajouté en favoris'),
        ),
      )
    },
    child: Column(
      children: [
        Stack(
          children: [
            Positioned(
              left: 30.0,
              child: new Container(
                margin: EdgeInsets.symmetric(vertical: 25),
                width: 35,
                height: 35,
                decoration: new BoxDecoration(
                  shape: BoxShape.circle,
                  image: new DecorationImage(
                    fit: BoxFit.cover,
                    image: new NetworkImage(
                      img,
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              left: 80.0,
              child: new Container(
                margin: EdgeInsets.only(top: 30),
                child: Text(
                  info[i].name,
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Container(
              padding: EdgeInsetsDirectional.only(top: 50),
              child: SizedBox(
                height: 300,
                width: double.infinity,
                child: Card(
                  semanticContainer: true,
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  child: Image.network(
                    img,
                    fit: BoxFit.fill,
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(15.0),
                        topRight: Radius.circular(15.0)),
                  ),
                  elevation: 5,
                  margin: EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0),
                ),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 50.0,
          width: double.infinity,
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 15),
            decoration: BoxDecoration(
              border: Border(
                top: BorderSide(
                    width: 2, color: Colors.black, style: BorderStyle.solid),
                bottom: BorderSide(
                    width: 2, color: Colors.black, style: BorderStyle.solid),
                left: BorderSide(
                    width: 2, color: Colors.black, style: BorderStyle.solid),
                right: BorderSide(
                    width: 2, color: Colors.black, style: BorderStyle.solid),
              ),
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(15.0),
                bottomRight: Radius.circular(15.0),
              ),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsetsDirectional.only(start: 10),
                    ),
                    Icon(
                      Icons.favorite,
                      color: Colors.grey,
                      size: 35.0,
                    ),
                    Padding(
                      padding: EdgeInsetsDirectional.only(start: 10),
                    ),
                    Icon(
                      Icons.add_comment,
                      color: Colors.black,
                      size: 35.0,
                    ),
                    Padding(
                      padding: EdgeInsetsDirectional.only(start: 10),
                    ),
                    Text(
                      info[i].size,
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20.0),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      info[i].price + ' €',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20.0),
                    ),
                    Padding(
                      padding: EdgeInsetsDirectional.only(end: 10),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    ),
  );
}
